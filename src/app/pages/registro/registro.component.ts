import { Component, OnInit } from '@angular/core';
import { UserModel } from '../../models/user.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  user: UserModel = new UserModel();

  constructor() { }

  ngOnInit() {
  }

  OnSubmit(form: NgForm) {

    if (form.invalid) {
      return;
    }
    console.log('Formulario Enviado');
    console.log(this.user);
    console.log(form);


  }


}
