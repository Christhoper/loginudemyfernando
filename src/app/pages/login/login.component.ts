import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserModel } from '../../models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: UserModel = new UserModel();

  constructor() { }

  ngOnInit() {
  }

  OnSubmit(form: NgForm) {

    if (form.invalid) {
      return;
    }
    console.log('Formulario Enviado');
    console.log(this.user);
    console.log(form);


  }

}
