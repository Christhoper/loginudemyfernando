import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { UserModel } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url: 'https://identitytoolkit.googleapis.com/v1';
  private apiKey: 'AIzaSyCHmTm8VxrH1s4eDPnwNkuzAVH2lhAsQDk';
  // Crear nuevos Usuarios
  // https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]

  // Login
  // https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=[API_KEY]

  constructor( private httpCliente: HttpClientModule ) { }

  logout(){

  }

  login( user: UserModel){

  }

  registerNewUser( user: UserModel){

  }
}
